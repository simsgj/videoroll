package com.eyecu.ane.ios.videoroll{
	import flash.events.EventDispatcher;
	import flash.external.ExtensionContext;
	import flash.events.StatusEvent;
	import flash.utils.ByteArray;
	import flash.display.BitmapData;
	
	public class VideoRoll extends EventDispatcher{
		private var _extContext:ExtensionContext;
		private static var _instance:VideoRoll;
		private var _batchImageArray:Array;
		
		public function VideoRoll(enf:SingletonEnforcer){
			_extContext = ExtensionContext.createExtensionContext("com.eyecu.ane.ios.videoroll.VideoRoll", "" );
			_extContext.addEventListener( StatusEvent.STATUS, onStatus );
		}
		
		public static function get instance():VideoRoll {
			if ( !_instance ){
				_instance = new VideoRoll( new SingletonEnforcer() );
				_instance.init();
			}
			return _instance;
		}
		
		private function onStatus(e:StatusEvent):void{
			var response:Object = JSON.parse(e.level);
			if(response.hasOwnProperty("sendBytes")){
				switch(response["sendBytes"]){
					case "0":
						this.dispatchVideoSelect(response["videoUrl"], response["length"]);
					break;
					
					case "1":
						this.askForVideoBitmapData(response['width'], response['height'], response["videoUrl"], response["length"]);
					break;
				}
			} else if(response.hasOwnProperty("videoStart")){
				_batchImageArray = new Array();
			} else if (response.hasOwnProperty("videoProgress")){
				this.addImageUrlToArray(response['videoProgress']);
			} else if (response.hasOwnProperty("videoComplete")){
				this.dispatchBatchVideoCompleteEvent();
			}
			trace("on status: " + e.code + " " + e.level);
		}
		
		private function addImageUrlToArray(imageUrl:String):void{
			_batchImageArray.push(imageUrl);
		}
		
		private function dispatchBatchVideoCompleteEvent():void{
			var dispatch:VideoRollEvent = new VideoRollEvent(VideoRollEvent.ON_BATCH_THUMBNAILS_COMPLETE);
			dispatch.batchVideoThumbUrls = _batchImageArray;
			dispatchEvent(dispatch);
		}

        public function getBitmapFromVideoAtTime(time:Number, width:Number, height:Number, cleanup:Boolean):BitmapData{
           	var bmd:BitmapData = new BitmapData(width, height);
            _extContext.call("getBitmapFromVideoAtTime", time, bmd, cleanup);
 			return bmd;
        }
        
        public function getBitmapFromVideoAtTimes(times:Array, width:Number, height:Number):void{
            _extContext.call("getBitmapFromVideoAtTimes", times, width, height);
        }
		
		private function askForVideoBitmapData(width:Number, height:Number, url:String, length:Number):void{
			var ba:ByteArray = new ByteArray();
			var bmd:BitmapData = new BitmapData(width, height);
			_extContext.call("askForBinaryVideoData", ba, bmd);
			this.dispatchVideoPromise(ba, bmd, url, length);

		}
		
		private function dispatchVideoPromise(ba:ByteArray, bmd:BitmapData, url:String, length:Number):void{
			var dispatch:VideoRollEvent = new VideoRollEvent(VideoRollEvent.ON_VIDEO_PROMISE);
			dispatch.videoThumbnail = bmd;
			dispatch.videoUrl = url;
			dispatch.videoData = ba;
            dispatch.videoLength = length;
			dispatchEvent(dispatch);
		}
		
		private function dispatchVideoSelect(url:String, length:Number):void{
			var dispatch:VideoRollEvent = new VideoRollEvent(VideoRollEvent.ON_VIDEO_SELECT);
			dispatch.videoUrl = url;
            dispatch.videoLength = length;
			dispatchEvent(dispatch);
			trace(url);
		}
		
		public function openVideoRoll(label:String = "Videos", allowTrim:Boolean = false, maxTrimLen:int = -1):void{
			_extContext.call("openVideoRoll", label, allowTrim, maxTrimLen);
		}
		
		public function openVideoRollReturnBytes(label:String = "Videos", allowTrim:Boolean = false, maxTrimLen:int = -1, cacheAsset:Boolean = false):void{
			_extContext.call("openVideoRollReturnBytes", label, allowTrim, maxTrimLen, cacheAsset);
		}
		
		private function init():void{
			_extContext.call("init");
		}
		
	}

}

class SingletonEnforcer{ }