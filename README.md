# Video Roll - an iOS ANE for accessing saved videos. #
This code is still very much in development and should not be considered production ready!

Questions/issues/comments/improvements?

`@frankaltomare on Twitter`

`#actionscript on efnet`


### To Use ###
Include the VideoRoll.ane extension in your AIR iOS Project. Make sure to add it to your
projects application [descriptor][1]

### Currently Supports ###
Returning a URL to a selected video location.

Setting a custom label on the video roll screen.

Use of the native iOS video trimmer.

Setting a max length on a video.

Returning a video thumbnail as BitmapData.

Returning a video as a ByteArray.

Getting a thumbnail at a specific time.

Creating multiple thumbnails from an array of times. 



## Examples ##

### This extension is a singleton, do not create an instance directly. Instead use VideoRoll.instance ###


*Overview*

	protected function showVideoRoll():void {
		var videoRoll:VideoRoll = VideoRoll.instance; // Acquire an instance
		videoRoll.addEventListener(VideoRollEvent.ON_VIDEO_SELECT, handleVideoSelect); // Dispatched on video select
		videoRoll.openVideoRoll(); // Open the video roll
	}
	
	private function handleVideoSelect(e:VideoRollEvent):void { // this will not return a thumbnail or video ByteArray
		e.videoUrl //Video URL
		e.videoLength //Video Length
		var f:File = new File(e.videoUrl); // The location is returned in the event
		var ba:ByteArray = new ByteArray(); // Create a ByteArray to load the video into
		var fs:FileStream = new FileStream();
		fs.open(f, FileMode.READ); 
		fs.readBytes(ba, 0, fs.bytesAvailable); // Read the video into the BA
		fs.close();
	}
	
		
*Getting the video as a ByteArray with a Thumbnail*

Using `openVideoRollReturnBytes` will return a ByteArray with the video and BitmapData with a thumbnail.
		
	protected function showVidoeRollGetBinaryBack():void {
		var videoRoll:VideoRoll = VideoRoll.instance; // Acquire an instance
		videoRoll.addEventListener(VideoRollEvent.ON_VIDEO_PROMISE, handleVideoSelect); // This will be dispatched after we attempt
																						// to acquire video ByteArray and thumbnail BitmapData
		videoRoll.openVideoRollReturnBytes(); // Open the video roll, this will not dispatch a ON_VIDEO_SELECT event
	}
	
	private function handleVideoSelect(e:VideoRollEvent):void {
		e.videoThumbnail; // Thumbnail BitmapData
		e.videoData; // Video ByteArray
		e.videoUrl //Video URL
		e.videoLength //Video Length
	}
	
			
*Getting a thumbnail from a video at a time*

Setting the `cacheAsset` to  true with `openVideoRollReturnBytes` will cache the selected video. 
With this set you can use `getBitmapFromVideoAtTime` to get a thumbnail at any time.

	private function selectVideoKeepCache():void{
		 var videoRoll:VideoRoll = VideoRoll.instance;
		 //setting cacheAsset to true
		 videoRoll.openVideoRollReturnBytes("Select Video", true, 10, true);
	}


	private function getThumbnailAtTime():void{
		var bitmap:BitmapData;
		//time is in seconds
		//you can get the selected videos width/height off of VideoRollEvent.videoThumbnail 
		bitmap = _videoRoll.getBitmapFromVideoAtTime(5, 100, 100, false);
	}

To clear the stored cache set `cleanup` to true

	_image.source = _videoRoll.getBitmapFromVideoAtTime(5, _selectedWidth, _selectedHeight, /*cleanup */ true);
	

*Getting a an array of thumbnails from a single video*

This also requires setting `cacheAsset` to true and using `openVideoRollReturnBytes`.
Using `getBitmapFromVideoAtTimes` and passing in an array of times in seconds will create a thumbnail
from the cached video at each requested time.
On completion a `VideoRollEvent.ON_BATCH_THUMBNAILS_COMPLETE` is dispatched containing an array of thumbnail file locations.
The generated images are saved to a temporary directory.

	private function generatedThumbnailsAtTimes():void{
		// An array of times in seconds
		var timesArray:Array = [.2,.5,1,1.5,2,2.5,3]
		// ON_BATCH_THUMBNAILS_COMPLETE is dispatched when all requested thumbnails are complete
		_videoRoll.addEventListener(VideoRollEvent.ON_BATCH_THUMBNAILS_COMPLETE, onBatchImageComplete);
		// Pass in the array of times as well as the width and height of the generated thumbnails
		_videoRoll.getBitmapFromVideoAtTimes(timesArray, 360, 480);
	}
	
	private function onBatchImageComplete(e:VideoRollEvent):void{
        // This will hold an array of urls for each thumbnail
       	trace(e.batchVideoThumbUrls);
    }

	
###Options###

*Applies to both openVideoRoll and openVideoRollReturnBytes*

	protected function videoRollTitle():void {
		var videoRoll:VideoRoll = VideoRoll.instance;
		videoRoll.openVideoRoll("Fancy Title"); // Change the title of the video picker
	}

	protected function videoRollTrim():void {
		var videoRoll:VideoRoll = VideoRoll.instance;
		videoRoll.openVideoRoll("Fancy Title", true); // Allow use of the native iOS video trimmer
	}
	
	protected function videoRollTrimSetLen():void {
		var videoRoll:VideoRoll = VideoRoll.instance;
		videoRoll.openVideoRoll("Fancy Title", true, 10); // Set a max video length
														  // This forces the user to trim a selected video down to size
	}


[1]:http://help.adobe.com/en_US/air/build/WS597e5dadb9cc1e0253f7d2fc1311b491071-8000.html#WS08cc5e527b0868243ea2ffcd1314dff873a-7ffe




